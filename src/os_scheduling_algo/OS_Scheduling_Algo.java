/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_scheduling_algo;

import java.util.Scanner;

/**
 *
 * @author vinay
 */
public class OS_Scheduling_Algo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner inp = new Scanner(System.in) ;
        System.out.println(" 1. Shortest Job First (SJF)\n 2. Shortest Remaining Time First (SRTF)\n 3. Priority (Non-Preemptive)\n 4. Priority (Preemptive)\n Enter Choice Number:");
        int choice = inp.nextInt();
        System.out.println("Number of Processes:");
        int noOfProcesses = inp.nextInt();
        Scheduler scheduler = null;
        switch(choice) {
            case 1:
            case 2:
                if(choice == 1)
                    scheduler = new SJFScheduler();
                else
                    scheduler = new SRTFScheduler();
                System.out.println("ProcessID  ArrivalTime CPUBurstTime");
                for (int i = 0; i < noOfProcesses; i++) {
                    System.out.print(i+"  ");
                    float at = inp.nextFloat();
                    float bt = inp.nextFloat();
                    
                    scheduler.addToReadyQueue(new Process(at, bt));
                }
                break;
            case 3:
            case 4:
                System.out.println("1. Low Number As HIGH Priority\n2.High Number AS HIGH Priority");
                boolean lowAsHigh = false;
                if(inp.nextInt() == 1)
                    lowAsHigh = true;
                
                if(choice == 3)
                    scheduler = new PriorityPreemptiveScheduler(lowAsHigh);
                else
                    scheduler = new PriorityPreemptiveScheduler(lowAsHigh);
                System.out.println("ProcessID  ArrivalTime CPUBurstTime  Priority");
                for (int i = 0; i < noOfProcesses; i++) {
                    System.out.print(i+"  ");
                    float at = inp.nextFloat();
                    float bt = inp.nextFloat();
                    int priority = inp.nextInt();
                    scheduler.addToReadyQueue(new PriorityProcess(at, bt, priority));
                }
                break;
            
        } 
        try {
            scheduler.schedule();
            scheduler.printStats();
        }
        catch(Exception e) {
            System.err.println("Choice not selected properly");
        }
    }
}
