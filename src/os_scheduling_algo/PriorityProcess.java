/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_scheduling_algo;

/**
 *
 * @author vinay
 */
public class PriorityProcess extends Process{
    private int priority;
    public PriorityProcess() {
        super();
        priority = Integer.MAX_VALUE;
    }
    
    public PriorityProcess(float arrivalTime, float CPUBurstTime) {
        super(arrivalTime, CPUBurstTime);
        priority = Integer.MAX_VALUE;
    }
    
    public PriorityProcess(float arrivalTime, float CPUBurstTime, int priority) {
        super(arrivalTime, CPUBurstTime);
        this.priority = priority;
    }
    
    public PriorityProcess(int processID, float arrivalTime, float CPUBurstTime, int priority) throws Exception {
        super(processID, arrivalTime, CPUBurstTime);
        this.priority = priority; 
    }
    
    public int getPriority() {
        return this.priority;
    }
}
