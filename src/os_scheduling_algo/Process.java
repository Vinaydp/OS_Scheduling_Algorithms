/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_scheduling_algo;

/**
 *
 * @author vinay
 */
public class Process {
    static  int nextProcessID = 0;
    private int processID;
    private float arrivalTime, CPUBurstTime, BurstTimeLeft;
    public float turnAroundTime, waitingTime, completionTime, responseTime, startTime;
    public boolean completed, started;
    public Process() {
        this.processID = nextProcessID;
        nextProcessID++;
        completed = false;
    }
    
    public Process(float arrivalTime, float CPUBurstTime) {
        this.processID = nextProcessID;
        this.arrivalTime = arrivalTime;
        this.CPUBurstTime = CPUBurstTime;
        this.BurstTimeLeft = CPUBurstTime;
        completed = false;
        nextProcessID++;
    }
    
    public Process(int processID, float arrivalTime, float CPUBurstTime) throws Exception {
        if(processID < nextProcessID)
            throw new Exception("Process ID might have been used before for other process");
        this.processID = processID;
        this.arrivalTime = arrivalTime;
        this.CPUBurstTime = CPUBurstTime;
        this.BurstTimeLeft = CPUBurstTime; 
        completed = false;
        nextProcessID = processID + 1;
    }
    
    public float getArrivalTime() {
        return this.arrivalTime;
    }
    
    public float getRemainingBurst() {
        return this.BurstTimeLeft;
    }
    
    public float getCPUBurstTime(){
        return this.CPUBurstTime;
    }
    public float runFor(float time) { //runs for time specified
        return (this.BurstTimeLeft= this.BurstTimeLeft - time) ;
    }
    public int getID() {
        return processID;
    }
}
