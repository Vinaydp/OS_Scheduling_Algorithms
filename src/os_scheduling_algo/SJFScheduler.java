/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_scheduling_algo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 *
 * @author vinay
 */
public class SJFScheduler implements Scheduler {
    ArrayList<Process> readyQueue;
    Comparator<Process> comparator, burstComparator;
    int index;
    float minArrivalTime, maxTime, scheduleLength, avgWaitingTime, avgTurnAroundTime, throughput, CPUUtilization;
    public SJFScheduler() {
        readyQueue = new ArrayList<>();
        comparator = (Process o1, Process o2) -> (o1.getArrivalTime()< o2.getArrivalTime()? -1 : o1.getArrivalTime() > o2.getArrivalTime()? 1 : 0) // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        ;
        
        burstComparator = (Process o1, Process o2) -> (o1.getRemainingBurst()< o2.getRemainingBurst()? -1 : o1.getRemainingBurst() > o2.getRemainingBurst()? 1 : 0) //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        ;
    }
    
    @Override
    public void schedule(){
        int noOfProcesses = readyQueue.size();
        if(noOfProcesses == 0)
            return;
        readyQueue.sort(comparator);
        minArrivalTime = readyQueue.get(0).getArrivalTime();
        
        index = 0;
        Queue<Process> burstQueue = new PriorityQueue<>(burstComparator);
        float time = 0;
        ArrayList<Float> intervals;
        intervals = new ArrayList<>();
      //  ArrayList<Process> ganttChart = new ArrayList<>();
        //intervals.add(new Float(0));
        Process t ;
        int noOfProcessDone = 0;
        System.out.println("GANTT CHART :");
        System.out.print("\n |");
        while(noOfProcessDone < noOfProcesses) {
            
            while(index < noOfProcesses) { 
                t = (Process)readyQueue.get(index);
                if(t.getArrivalTime() <= time) {
                    burstQueue.add(t);
                    index++;
                }
                else {
                    break;
                }
            }//Get all process which arrive till "time"
           
            t = (Process)burstQueue.poll();//"get Minimum burst time"
            if(t != null) {
                t.startTime = time;
                time += t.getRemainingBurst();
                t.completed = true;
                t.completionTime = time;
                t.runFor(t.getRemainingBurst());
                noOfProcessDone++;
                intervals.add(time);
                System.out.print("   P"+t.getID()+"   |");
            }
            else if(t == null && (index < noOfProcesses)) {//process arrives little late
                System.out.print("   GP   |");
                float addTime = readyQueue.get(index).getArrivalTime();
                intervals.add(addTime);
                time = addTime;
            }
            else {
                System.out.println("Not come here");
            }
        }
        System.out.print("\n0.0");
        
        for (java.lang.Float interval : intervals) {
            //System.out.print("    ");
            System.out.printf("%9.1f", interval);
        }
        
        maxTime = time;
        scheduleLength = maxTime - minArrivalTime;
        throughput = (float)noOfProcesses / scheduleLength;
   
        float totalTurnAroundTime = 0;
        float totalWaitingTime = 0;
        float totalBurst = 0;
        for(Process p : readyQueue){
            p.turnAroundTime = p.completionTime - p.getArrivalTime();
            p.waitingTime = p.turnAroundTime - p.getCPUBurstTime();
            p.responseTime = p.startTime - p.getArrivalTime();
            totalTurnAroundTime += p.turnAroundTime;
            totalWaitingTime += p.waitingTime;
            totalBurst += p.getCPUBurstTime();
        }
        
        avgTurnAroundTime = totalTurnAroundTime/ noOfProcesses;
        avgWaitingTime = totalWaitingTime / noOfProcesses;
        CPUUtilization = totalBurst/maxTime;
    }
    @Override
    public void addToReadyQueue(Process p){
        readyQueue.add(p);
        
    }
    @Override
    public void printStats() {
        
        System.out.println("\n===========================================================================================================");
        System.out.println("Process ID    Arrival Time    Burst Time    Completion Time    TurnAround    Waiting Time    Response Time");
        System.out.println("\n===========================================================================================================");
        for(Process p : readyQueue){
            
            System.out.printf("%9s ", "P"+p.getID());
            System.out.printf("%15.1f ", p.getArrivalTime());
            System.out.printf("%13.1f ", p.getCPUBurstTime());
            System.out.printf("%18.1f ", p.completionTime);
            System.out.printf("%13.1f ", p.turnAroundTime);
            System.out.printf("%14.1f ", p.waitingTime);
            System.out.printf("%15.1f ", p.responseTime);
            System.out.println();
        }
        System.out.println("\n===========================================================================================================");
        System.out.printf(" Schedule Length: %.3f\n Average Turn Aroud time = %.3f\n Average Waiting Time = %.3f\n Throughput = %.3f\n CPU Utilization : %.3f %%\n", scheduleLength,  avgTurnAroundTime, avgWaitingTime, throughput, (CPUUtilization * 100));
        
    }
}
