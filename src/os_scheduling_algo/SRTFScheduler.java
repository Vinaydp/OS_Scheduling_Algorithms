/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_scheduling_algo;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 *
 * @author vinay
 */
public class SRTFScheduler extends SJFScheduler{
    @Override
    public void schedule(){
        int noOfProcesses = readyQueue.size();
        if(noOfProcesses == 0)
            return;
        readyQueue.sort(comparator);
        minArrivalTime = readyQueue.get(0).getArrivalTime();
        
        index = 0;
        Queue<Process> burstQueue = new PriorityQueue<>(burstComparator);
        float time = 0;
        ArrayList<Float> intervals;
        intervals = new ArrayList<>();
      //  ArrayList<Process> ganttChart = new ArrayList<>();
        //intervals.add(new Float(0));
        Process t ;
        int noOfProcessDone = 0;
        System.out.println("GANTT CHART :");
        System.out.print("\n |");
        while(noOfProcessDone < noOfProcesses) {
            
            while(index < noOfProcesses) { 
                t = (Process)readyQueue.get(index);
                if(t.getArrivalTime() <= time) {
                    burstQueue.add(t);
                    index++;
                }
                else {
                    break;
                }
            }//Get all process which arrive till "time"
           
            t = (Process)burstQueue.poll();//"get Minimum burst time"
            
            if(t != null) {
                //t.startTime = time;
                if(!t.started) { /*Process was not started but now started*/
                    t.startTime = time;
                    t.started = true;
                }
                float runForTime;
                if(index < noOfProcesses) {/*Some Processes may arrive later on*/
                    float nextArrivalTime = readyQueue.get(index).getArrivalTime(); /*All process done or not*/
                    //System.out.println("nAT--"+nextArrivalTime);
                    runForTime = Float.min(nextArrivalTime - time, t.getRemainingBurst());
                }
                else {
                    runForTime = t.getRemainingBurst();
                }
                
                time += runForTime;
                
                if(t.runFor(runForTime) == 0) { /*Process ran completely*/
                    t.completed = true;
                    t.completionTime = time;
                    noOfProcessDone++;
                }
                else {//Some left add the process back to queue
                    burstQueue.add(t);
                }
                intervals.add(time);
                System.out.print("   P"+t.getID()+"   |");
                
            }
            else if(t == null && (index < noOfProcesses)) {//process arrives little late
                System.out.print("   GP   |");
                float addTime = readyQueue.get(index).getArrivalTime();
                intervals.add(addTime);
                time = addTime;
            }
            else {
                System.out.println("Not come here");
            }
        }
        System.out.print("\n0.0");
        
        for (java.lang.Float interval : intervals) {
            //System.out.print("    ");
            System.out.printf("%9.1f", interval);
        }
        
        maxTime = time;
        scheduleLength = maxTime - minArrivalTime;
        throughput = (float)noOfProcesses / scheduleLength;
   
        float totalTurnAroundTime = 0;
        float totalWaitingTime = 0;
        float totalBurst = 0;
        for(Process p : readyQueue){
            p.turnAroundTime = p.completionTime - p.getArrivalTime();
            p.waitingTime = p.turnAroundTime - p.getCPUBurstTime();
            p.responseTime = p.startTime - p.getArrivalTime();
            totalTurnAroundTime += p.turnAroundTime;
            totalWaitingTime += p.waitingTime;
            totalBurst += p.getCPUBurstTime();
        }
        
        avgTurnAroundTime = totalTurnAroundTime/ noOfProcesses;
        avgWaitingTime = totalWaitingTime / noOfProcesses;
        CPUUtilization = totalBurst/maxTime;
    }
}
