/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_scheduling_algo;

/**
 *
 * @author vinay
 */
public interface Scheduler {
    public void schedule();
    public void addToReadyQueue(Process p);
    public void printStats();
}
